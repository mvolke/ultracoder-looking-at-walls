
var animEndEventNames = {
    'WebkitAnimation' : 'webkitAnimationEnd',
    'MozAnimation' : 'animationend',
    'OAnimation' : 'oAnimationEnd',
    'msAnimation' : 'MSAnimationEnd',
    'animation' : 'animationend'
};
var animEndEventName = animEndEventNames[Modernizr.prefixed('animation')];

var modernBrowser = Modernizr.csstransforms3d && Modernizr.cssanimations;

function PageView(el) {
    this.$el = $(el);
}

PageView.prototype.animate = function(trans, cb) {
    var that = this;
    this.$el.addClass(trans)
    .on(animEndEventName, function() {
        that.$el.off(animEndEventName);
        that.$el.removeClass(trans);
        if(cb) cb();
    });
};
PageView.prototype.show = function() {
    this.$el.css('visibility', 'visible');
};
PageView.prototype.hide = function() {
    this.$el.css('visibility', 'hidden');
};
PageView.prototype.transitionIn = function(back) {
    this.show();

    if(modernBrowser) {
        this.animate(back ? 'page-trans-back-in' : 'page-trans-in');
    }
    else {
        this.$el.css('left', (back ? -1 : 1) * this.$el.width());
        this.$el.animate({ left: 0 }, 1000);
    }
};

PageView.prototype.transitionOut = function(back) {
    var that = this;
    var cb = function() {
        that.hide();
    };

    if(modernBrowser) {
        this.animate(
            back ? 'page-trans-back-out': 'page-trans-out',
            cb
        );
    }
    else {
        this.$el.animate({ left: (back ? 1 : -1) * this.$el.width() }, 1000, cb);
    }
};

$(function() {

    var $pages = $('.page');

    var pages = [];
    $pages.each(function() {
        var page = new PageView(this);
        page.hide();
        pages.push(page);
    });
    var p = 0;
    pages[p].show();

    $(window).keydown(function(e) {
        if(e.which === 37) {
            pages[p].transitionOut(true);
            p = (p+pages.length-1) % pages.length;
            pages[p].transitionIn(true);
        }
        else if(e.which === 39) {
            pages[p].transitionOut();
            p = (p+1) % pages.length;
            pages[p].transitionIn();
        }
    });

});