/*global module:false*/
module.exports = function(grunt) {

    grunt.initConfig({
        autoprefixer: {
            options: {
                browsers: ['last 2 versions', 'ie 8']
            },
            dist: {
                files: {
                    'css/dist.css': 'css/main.css'
                }
            }
        },
        watch: {
            css: {
                files: ['css/main.css'],
                tasks: ['autoprefixer']
            }
        }
    });

    grunt.loadNpmTasks('grunt-autoprefixer');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['autoprefixer']);
};